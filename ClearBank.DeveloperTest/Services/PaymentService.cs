﻿using System.Collections.Generic;
using ClearBank.DeveloperTest.Data;
using ClearBank.DeveloperTest.Types;
using System.Configuration;
using ClearBank.DeveloperTest.Helpers;
using ClearBank.DeveloperTest.Wrappers;

namespace ClearBank.DeveloperTest.Services
{
    public interface IPaymentService
    {
        MakePaymentResult MakePayment(MakePaymentRequest request);
    }

    public class PaymentService : IPaymentService
    {
        private readonly IAccountDataStore _accountDataStore;
        private readonly IPaymentValidationHelper _paymentValidationHelper;

        public PaymentService(IAccountDataStore accountDataStore, IPaymentValidationHelper paymentValidationHelper)
        {
            _accountDataStore = accountDataStore;
            _paymentValidationHelper = paymentValidationHelper;
        }

        public MakePaymentResult MakePayment(MakePaymentRequest request)
        {
            var account = _accountDataStore.GetAccount(request.DebtorAccountNumber);

            if (account == null)
            {
                return new MakePaymentResult()
                {
                    Success = false,
                    Errors = new List<string>(){ $"Cannot retrieve account {request.DebtorAccountNumber}" }
                };
            }

            var result = _paymentValidationHelper.Validate(request, account);

            if (result.Success)
            {
                account.Balance -= request.Amount;

                _accountDataStore.UpdateAccount(account);
            }

            return result;
        }
    }
}
