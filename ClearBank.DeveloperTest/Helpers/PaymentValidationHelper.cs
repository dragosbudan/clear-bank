﻿using System.Collections.Generic;
using System.Collections.Specialized;
using ClearBank.DeveloperTest.Types;

namespace ClearBank.DeveloperTest.Helpers
{
    public interface IPaymentValidationHelper
    {
        MakePaymentResult Validate(MakePaymentRequest request, Account account);
    }

    public class PaymentValidationHelper : IPaymentValidationHelper
    {
        public MakePaymentResult Validate(MakePaymentRequest request, Account account)
        {
            var errors = new List<string>();
            //considered refactoring this switch to a single HasFlag call (convert payment scheme to binary and then cast into allowed payment schemes)
            //but didnt like how it looked so left it like this, although a bit repetitive
            switch (request.PaymentScheme)
            {
                case PaymentScheme.Bacs:
                    if (!account.AllowedPaymentSchemes.HasFlag(AllowedPaymentSchemes.Bacs))
                    {
                        errors.Add($"Payment scheme '{request.PaymentScheme.ToString()}' is not allowed on this account");
                    }
                    break;

                case PaymentScheme.FasterPayments:
                    if (!account.AllowedPaymentSchemes.HasFlag(AllowedPaymentSchemes.FasterPayments))
                    {
                        errors.Add($"Payment scheme '{request.PaymentScheme.ToString()}' is not allowed on this account");
                    }
                    break;

                case PaymentScheme.Chaps:
                    if (!account.AllowedPaymentSchemes.HasFlag(AllowedPaymentSchemes.Chaps))
                    {
                        errors.Add($"Payment scheme '{request.PaymentScheme.ToString()}' is not allowed on this account");
                    }
                    break;
            }

            if (account.Status != AccountStatus.Live)
            {
                errors.Add("Account is not able to make outbound payments");
            }

            if (account.Balance < request.Amount)
            {
                errors.Add("There is not enough money in the account");
            }

            return new MakePaymentResult()
            {
                Success = errors.Count == 0,
                Errors = errors
            };
        }
    }
}