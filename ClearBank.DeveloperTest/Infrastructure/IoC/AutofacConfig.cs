﻿using Autofac;

namespace ClearBank.DeveloperTest.Infrastructure.IoC
{
    public static class AutoFacConfig
    {
        //call this registration method at startup
        public static void Register()
        {
            var builder = new ContainerBuilder();
            builder.RegisterAssemblyModules(typeof(BaseModule).Assembly);

            var container = builder.Build();

            //!set the dependency resolver to use this autofac container for your chosen technology
        }
    }
}