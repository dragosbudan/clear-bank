﻿using System;
using Moq;
using Ploeh.AutoFixture;
using Ploeh.AutoFixture.AutoMoq;

namespace ClearBank.DeveloperTest.Tests.Infrastructure
{
    public class ClassContext<T> where T : class
    {
        private static Lazy<T> _classFactory;
        private static IFixture _fixture;

        public ClassContext()
        {
            _fixture = new Fixture().Customize(new AutoMoqCustomization());
            _classFactory = new Lazy<T>(() => _fixture.Create<T>());
        }

        protected static Mock<TInterface> MockOf<TInterface>() where TInterface : class
        {
            if (_classFactory.IsValueCreated)
            {
                throw new InvalidOperationException(" You can't freeze any more after the SUT has been accessed. " +
                                                    "Do all of the Mocking before accessing the SUT. " +
                                                    "Trying to Mock the " + typeof(TInterface).Name + " type");
            }

            var mock = _fixture.Freeze<Mock<TInterface>>();
            return mock;
        }

        protected static T SUT
        {
            get { return _classFactory.Value; }
        }
    }
}
