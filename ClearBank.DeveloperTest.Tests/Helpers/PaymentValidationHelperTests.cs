﻿using ClearBank.DeveloperTest.Helpers;
using ClearBank.DeveloperTest.Tests.Infrastructure;
using ClearBank.DeveloperTest.Types;
using Machine.Specifications;
using Shouldly;

namespace ClearBank.DeveloperTest.Tests.Helpers
{
    public class when_payment_validated_successfully : ClassContext<PaymentValidationHelper>
    {
        private Because of = () =>
        {
            _result = SUT.Validate(_request, _account);
        };

        private It should_return_successful_payment_result = () =>
        {
            _result.Success.ShouldBe(true);
            _result.Errors.Count.ShouldBe(0);
        };

        private Establish that = () =>
        {
            _request = new MakePaymentRequest()
            {
                PaymentScheme = PaymentScheme.Bacs,
                Amount = 100
            };

            _account = new Account()
            {
                AllowedPaymentSchemes = AllowedPaymentSchemes.Bacs | AllowedPaymentSchemes.Chaps | AllowedPaymentSchemes.FasterPayments,
                Balance = 200,
                Status = AccountStatus.Live
            };
        };

        private static MakePaymentResult _result;
        private static MakePaymentRequest _request;
        private static Account _account;
    }

    public class when_validating_payment_and_payment_scheme_is_not_allowed : ClassContext<PaymentValidationHelper>
    {
        private Because of = () =>
        {
            _result = SUT.Validate(_request, _account);
        };

        private It should_return_unsuccessful_payment_result = () =>
        {
            _result.Success.ShouldBe(false);
            _result.Errors.ShouldNotBeNull();
            _result.Errors[0].ShouldBe("Payment scheme 'Bacs' is not allowed on this account");
        };

        private Establish that = () =>
        {
            _request = new MakePaymentRequest()
            {
                PaymentScheme = PaymentScheme.Bacs,
                Amount = 100
            };

            _account = new Account()
            {
                AllowedPaymentSchemes = AllowedPaymentSchemes.Chaps | AllowedPaymentSchemes.FasterPayments,
                Balance = 200,
                Status = AccountStatus.Live
            };
        };

        private static MakePaymentResult _result;
        private static MakePaymentRequest _request;
        private static Account _account;
    }

    public class when_validating_payment_and_requested_amount_is_less_than_balance : ClassContext<PaymentValidationHelper>
    {
        private Because of = () =>
        {
            _result = SUT.Validate(_request, _account);
        };

        private It should_return_unsuccessful_payment_result = () =>
        {
            _result.Success.ShouldBe(false);
            _result.Errors.Count.ShouldBe(1);
            _result.Errors[0].ShouldBe("There is not enough money in the account");
        };

        private Establish that = () =>
        {
            _request = new MakePaymentRequest()
            {
                PaymentScheme = PaymentScheme.Bacs,
                Amount = 100
            };

            _account = new Account()
            {
                AllowedPaymentSchemes = AllowedPaymentSchemes.Bacs | AllowedPaymentSchemes.Chaps | AllowedPaymentSchemes.FasterPayments,
                Balance = 99,
                Status = AccountStatus.Live
            };
        };

        private static MakePaymentResult _result;
        private static MakePaymentRequest _request;
        private static Account _account;
    }

    public class when_validating_payment_and_account_status_is_not_live : ClassContext<PaymentValidationHelper>
    {
        private Because of = () =>
        {
            _result = SUT.Validate(_request, _account);
        };

        private It should_return_unsuccessful_payment_result = () =>
        {
            _result.Success.ShouldBe(false);
            _result.Errors.Count.ShouldBe(1);
            _result.Errors[0].ShouldBe("Account is not able to make outbound payments");
        };

        private Establish that = () =>
        {
            _request = new MakePaymentRequest()
            {
                PaymentScheme = PaymentScheme.Bacs,
                Amount = 100
            };

            _account = new Account()
            {
                AllowedPaymentSchemes = AllowedPaymentSchemes.Bacs | AllowedPaymentSchemes.Chaps | AllowedPaymentSchemes.FasterPayments,
                Balance = 200,
                Status = AccountStatus.InboundPaymentsOnly
            };
        };

        private static MakePaymentResult _result;
        private static MakePaymentRequest _request;
        private static Account _account;
    }

    public class when_validating_payment_and_there_is_more_than_one_error : ClassContext<PaymentValidationHelper>
    {
        private Because of = () =>
        {
            _result = SUT.Validate(_request, _account);
        };

        private It should_return_unsuccessful_payment_result = () =>
        {
            _result.Success.ShouldBe(false);
            _result.Errors.Count.ShouldBe(3);
            _result.Errors.ShouldContain("Account is not able to make outbound payments");
            _result.Errors.ShouldContain("There is not enough money in the account");
            _result.Errors.ShouldContain("Payment scheme 'Bacs' is not allowed on this account");
        };

        private Establish that = () =>
        {
            _request = new MakePaymentRequest()
            {
                PaymentScheme = PaymentScheme.Bacs,
                Amount = 250
            };

            _account = new Account()
            {
                AllowedPaymentSchemes = AllowedPaymentSchemes.Chaps | AllowedPaymentSchemes.FasterPayments,
                Balance = 200,
                Status = AccountStatus.Disabled
            };
        };

        private static MakePaymentResult _result;
        private static MakePaymentRequest _request;
        private static Account _account;
    }
}
