﻿using System.Collections.Generic;
using ClearBank.DeveloperTest.Data;
using ClearBank.DeveloperTest.Helpers;
using ClearBank.DeveloperTest.Services;
using ClearBank.DeveloperTest.Tests.Infrastructure;
using ClearBank.DeveloperTest.Types;
using Machine.Specifications;
using Moq;
using Shouldly;
using It = Machine.Specifications.It;

namespace ClearBank.DeveloperTest.Tests.Services
{
    public class when_making_a_successful_payment : ClassContext<PaymentService>
    {
        private Because of = () =>
        {
            _result = SUT.MakePayment(_request);
        };

        private It should_call_the_account_data_store = () =>
        {
            _accountDataStore.Verify(x => x.GetAccount(_accountNumber), Times.Once);
        };

        private It should_validate_the_payment_request = () =>
        {
            _paymentValidationHelper.Verify(x => x.Validate(_request, _account), Times.Once);
        };

        private It should_update_account_details = () =>
        {
            _accountDataStore.Verify(x => x.UpdateAccount(Moq.It.Is<Account>(y => y.Balance == 150)), Times.Once);
        };

        private It should_return_successful_result = () =>
        {
            _result.Success.ShouldBe(true);
            _result.Errors.Count.ShouldBe(0);
        };

        private Establish that = () =>
        {
            _account = new Account()
            {
                AccountNumber = _accountNumber,
                Balance = 200
            };

            _request = new MakePaymentRequest()
            {
                DebtorAccountNumber = _account.AccountNumber,
                Amount = 50
            };

            _accountDataStore = MockOf<IAccountDataStore>();
            _accountDataStore.Setup(x => x.GetAccount(_accountNumber))
                .Returns(_account);

            _paymentValidationHelper = MockOf<IPaymentValidationHelper>();
            _paymentValidationHelper.Setup(x => x.Validate(_request, _account))
                .Returns(new MakePaymentResult() { Success = true, Errors = new List<string>() });
        };

        private static string _accountNumber = "112";
        private static MakePaymentResult _result;
        private static Mock<IAccountDataStore> _accountDataStore;
        private static Account _account;
        private static MakePaymentRequest _request;
        private static Mock<IPaymentValidationHelper> _paymentValidationHelper;
    }

    public class when_making_a_payment_and_the_account_cannot_be_retrieved : ClassContext<PaymentService>
    {
        private Because of = () =>
        {
            _result = SUT.MakePayment(_request);
        };

        private It should_call_the_account_data_store = () =>
        {
            _accountDataStore.Verify(x => x.GetAccount(_accountNumber), Times.Once);
        };

        private It should_not_try_to_validate_the_payment_request = () =>
        {
            _paymentValidationHelper.Verify(x => x.Validate(Moq.It.IsAny<MakePaymentRequest>(), Moq.It.IsAny<Account>()), Times.Never);
        };

        private It should_not_update_account_details = () =>
        {
            _accountDataStore.Verify(x => x.UpdateAccount(Moq.It.IsAny<Account>()), Times.Never);
        };

        private It should_return_unsuccessful_result = () =>
        {
            _result.Success.ShouldBe(false);
            _result.Errors.Count.ShouldBe(1);
            _result.Errors[0].ShouldBe("Cannot retrieve account 112");
        };

        private Establish that = () =>
        {
            _request = new MakePaymentRequest()
            {
                DebtorAccountNumber = _accountNumber
            };

            _accountDataStore = MockOf<IAccountDataStore>();
            _accountDataStore.Setup(x => x.GetAccount(_accountNumber))
                .Returns(null as Account);

            _paymentValidationHelper = MockOf<IPaymentValidationHelper>();
        };

        private static string _accountNumber = "112";
        private static MakePaymentResult _result;
        private static Mock<IAccountDataStore> _accountDataStore;
        private static Account _account;
        private static MakePaymentRequest _request;
        private static Mock<IPaymentValidationHelper> _paymentValidationHelper;
    }

    public class when_making_a_payment_and_it_cannot_be_validated : ClassContext<PaymentService>
    {
        private Because of = () =>
        {
            _result = SUT.MakePayment(_request);
        };

        private It should_call_the_account_data_store = () =>
        {
            _accountDataStore.Verify(x => x.GetAccount(_accountNumber), Times.Once);
        };

        private It should_validate_the_payment_request = () =>
        {
            _paymentValidationHelper.Verify(x => x.Validate(_request, _account), Times.Once);
        };

        private It should_not_update_account_details = () =>
        {
            _accountDataStore.Verify(x => x.UpdateAccount(Moq.It.IsAny<Account>()), Times.Never);
        };

        private It should_return_unsuccessful_result = () =>
        {
            _result.Success.ShouldBe(false);
            _result.Errors.Count.ShouldBe(1);
            _result.Errors[0].ShouldBe("Payment not validated");
        };

        private Establish that = () =>
        {
            _account = new Account()
            {
                AccountNumber = _accountNumber,
                Balance = 200
            };

            _request = new MakePaymentRequest()
            {
                DebtorAccountNumber = _account.AccountNumber,
                Amount = 50
            };

            _accountDataStore = MockOf<IAccountDataStore>();
            _accountDataStore.Setup(x => x.GetAccount(_accountNumber))
                .Returns(_account);

            _paymentValidationHelper = MockOf<IPaymentValidationHelper>();
            _paymentValidationHelper.Setup(x => x.Validate(_request, _account))
                .Returns(new MakePaymentResult()
                {
                    Success = false,
                    Errors = new List<string>(){ "Payment not validated" }
                });
        };

        private static string _accountNumber = "112";
        private static MakePaymentResult _result;
        private static Mock<IAccountDataStore> _accountDataStore;
        private static Account _account;
        private static MakePaymentRequest _request;
        private static Mock<IPaymentValidationHelper> _paymentValidationHelper;
    }

}
