﻿using Autofac;
using ClearBank.DeveloperTest.Data;
using ClearBank.DeveloperTest.Services;
using ConfigurationManager = System.Configuration.ConfigurationManager;

namespace ClearBank.DeveloperTest.Infrastructure.IoC
{
    public class BaseModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            var dataStoreType = ConfigurationManager.AppSettings["DataStoreType"];

            if (dataStoreType == "Backup")
            {
                builder.RegisterType<BackupAccountDataStore>().As<IAccountDataStore>();
            }
            else
            {
                builder.RegisterType<AccountDataStore>().As<IAccountDataStore>();
            }

            builder.RegisterAssemblyTypes(typeof(PaymentService).Assembly)
                .Where(t =>
                    t.Name.EndsWith("Service") ||
                    t.Name.EndsWith("Helper") ||
                    t.Name.EndsWith("Wrapper"))
                .AsImplementedInterfaces();
        }
    }
}
