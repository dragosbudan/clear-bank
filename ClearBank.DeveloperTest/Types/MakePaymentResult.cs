﻿using System.Collections.Generic;

namespace ClearBank.DeveloperTest.Types
{
    public class MakePaymentResult
    {
        public bool Success { get; set; }
        public List<string> Errors { get; set; }
    }
}
