﻿using System.Configuration;

namespace ClearBank.DeveloperTest.Wrappers
{
    public interface IConfigurationManagerWrapper
    {
        string GetSetting(string key);
    }

    public class ConfigurationManagerWrapper : IConfigurationManagerWrapper
    {
        public string GetSetting(string key)
        {
            return ConfigurationManager.AppSettings[key];
        }
    }
}
